require "tomlrb"

Gem::Specification.new do |spec|
  spec.name = "sled-rb"
  spec.version = Tomlrb.load_file(File.join(__dir__, "Cargo.toml"))["package"]["version"]
  spec.authors = ["Kitaiti Makoto"]
  spec.email = ["KitaitiMakoto@gmail.com"]

  spec.summary = "A key-value store"
  spec.description = "Ruby bindings for sled, a modern embedded database written in Rust."
  spec.licenses = ["MIT", "Apache-2.0"]
  spec.homepage = "https://gitlab.com/KitaitiMakoto/sled-rb"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/KitaitiMakoto/sled-rb"
  spec.metadata["changelog_uri"] = "https://gitlab.com/KitaitiMakoto/sled-rb/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.extensions = ["ext/Rakefile"]

  spec.add_runtime_dependency "rutie"

  spec.add_development_dependency "rake"
  spec.add_development_dependency "rubygems-tasks"
  spec.add_development_dependency "test-unit"
  spec.add_development_dependency "yard"
  spec.add_development_dependency "tomlrb"
  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
