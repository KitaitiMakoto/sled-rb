require_relative "helper"
require "tmpdir"

class SledTest < Test::Unit::TestCase
  def test_crud
    Dir.mktmpdir do |dir|
      db = Sled.open(dir)

      db.clear
      assert_equal 0, db.length

      insert_res = db.insert("Hello", "World")
      assert_nil insert_res
      assert_equal 1, db.length

      get_res = db.get("Hello")
      assert_equal "World", get_res

      remove_res = db.remove("Hello");
      assert_equal "World", remove_res
      assert_nil db.get("Hello")
      assert_equal 0, db.length

      db.insert("Hello", "World")
      db.compare_and_swap("Hello", "World", "Again")
      assert_equal 1, db.length
      assert_equal "Again", db.get("Hello")

      assert_raise "CASError" do
        db.compare_and_swap("Hello", "World", "Again Again")
      end
      assert_equal "Again", db.get("Hello")
      assert_equal 1, db.length

      db.compare_and_swap("Hello", "Again", nil)
      assert_nil db.get("Hello")
      assert_equal 0, db.length

      db.compare_and_swap("Hello", nil, "Again Again Again")
      assert_equal "Again Again Again", db.get("Hello")
      assert_equal 1, db.length
    end
  end
end
