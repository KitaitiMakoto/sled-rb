use std::num::TryFromIntError;
use rutie::{
    class, methods, wrappable_struct, AnyException, AnyObject, Class, EncodingSupport, Exception,
    Integer, NilClass, Object, RString, VM,
};

struct Error(sled::Error);

impl From<sled::Error> for Error {
    fn from(err: sled::Error) -> Self {
        Self(err)
    }
}

impl From<Error> for AnyException {
    fn from(err: Error) -> AnyException {
        // TODO: Check error type
        match err.0 {
            _ => Self::new("RuntimeError", None),
        }
    }
}

struct CompareAndSwapError(sled::CompareAndSwapError);

impl From<sled::CompareAndSwapError> for CompareAndSwapError {
    fn from(err: sled::CompareAndSwapError) -> Self {
        Self(err)
    }
}

impl From<CompareAndSwapError> for AnyException {
    fn from (err: CompareAndSwapError) -> Self {
        Self::new("RuntimeError", Some("CASError")) // FIXME
    }
}

class!(Sled);
class!(Db);

wrappable_struct!(sled::Db, DbWrapper, DB_WRAPPER);

methods!(
    Sled,
    rtself,
    fn open(path: RString) -> Db {
        let path_string = path.map_err(VM::raise_ex).unwrap();
        let path_str = path_string.to_str();
        let db = sled::open(path_str)
            .map_err(Error::from)
            .map_err(VM::raise_ex)
            .unwrap();
        Class::from_existing("Sled")
            .get_nested_class("Db")
            .wrap_data(db, &*DB_WRAPPER)
    },
);

methods!(
    Db,
    rtself,
    fn clear() -> NilClass {
        let store = rtself.get_data(&*DB_WRAPPER);
        store
            .clear()
            .map_err(Error::from)
            .map_err(VM::raise_ex)
            .unwrap();
        NilClass::new()
    },
    fn insert(key: RString, value: RString) -> AnyObject {
        let key_string = key.map_err(VM::raise_ex).unwrap();
        let key_bytes = key_string.to_bytes_unchecked();
        let value_string = value.map_err(VM::raise_ex).unwrap();
        let value_bytes = value_string.to_bytes_unchecked();
        let store = rtself.get_data(&*DB_WRAPPER);
        store
            .insert(key_bytes, value_bytes)
            .map(|last_value| {
                last_value.map_or(NilClass::new().into(), |value| {
                    RString::from_bytes(&value, &value_string.encoding()).into()
                })
            })
            .map_err(Error::from)
            .map_err(VM::raise_ex)
            .unwrap()
    },
    fn get(key: RString) -> AnyObject {
        let key_string = key.map_err(VM::raise_ex).unwrap();
        let key_bytes = key_string.to_bytes_unchecked();
        let store = rtself.get_data(&*DB_WRAPPER);
        store
            .get(key_bytes)
            .map(|value| {
                value.map_or(NilClass::new().into(), |v| {
                    RString::from_bytes(&v, &key_string.encoding()).into()
                })
            })
            .map_err(Error::from)
            .map_err(VM::raise_ex)
            .unwrap()
    },
    fn remove(key: RString) -> AnyObject {
        let key_string = key.map_err(VM::raise_ex).unwrap();
        let key_bytes = key_string.to_bytes_unchecked();
        let store = rtself.get_data(&*DB_WRAPPER);
        store
            .remove(key_bytes)
            .map(|value| {
                value.map_or(NilClass::new().into(), |v| {
                    RString::from_bytes(&v, &key_string.encoding()).into()
                })
            })
            .map_err(Error::from)
            .map_err(VM::raise_ex)
            .unwrap()
    },
    fn compare_and_swap(key: RString, old: AnyObject, new: AnyObject) -> NilClass {
        let key_string = key.map_err(VM::raise_ex).unwrap();
        let key_bytes = key_string.to_bytes_unchecked();
        let old_object = old.map_err(VM::raise_ex).unwrap();
        let old_value = match old_object.is_nil() {
            true => None,
            false => {
                let old_rstring = old_object.try_convert_to::<RString>()
                    .map_err(VM::raise_ex)
                    .unwrap();
                Some(old_rstring.to_string_unchecked().into_bytes())
            }
        };
        let new_object = new.map_err(VM::raise_ex).unwrap();
        let mut new_value = match new_object.is_nil() {
            true => None,
            false => {
                let new_rstring = new_object.try_convert_to::<RString>()
                    .map_err(VM::raise_ex)
                    .unwrap();
                Some(new_rstring.to_string_unchecked().into_bytes())
            }
        };
        let store = rtself.get_data(&*DB_WRAPPER);
        store.compare_and_swap(key_bytes, old_value, new_value)
            .map_err(Error::from)
            .map_err(VM::raise_ex)
            .unwrap()
            .map_err(CompareAndSwapError::from)
            .map_err(VM::raise_ex)
            .unwrap();
        NilClass::new()
    },
    fn len() -> Integer {
        let db = rtself.get_data(&*DB_WRAPPER);
        Integer::new(db.len().try_into().map_err(|_| VM::raise_ex(AnyException::new("RuntimeError", Some("Failed to convert integer type")))).unwrap())
    }
);

#[allow(non_snake_case)]
#[no_mangle]
pub extern "C" fn Init_sled() {
    Class::new("Sled", None).define(|klass| {
        klass.def_self("open", open);
        klass.define_nested_class("Db", None);
        klass.get_nested_class("Db").define(|klass| {
            klass.def("clear", clear);
            klass.def("insert", insert);
            klass.def("get", get);
            klass.def("remove", remove);
            klass.def("compare_and_swap", compare_and_swap);
            klass.def("length", len);
        });
    });
}
